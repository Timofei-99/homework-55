import React from 'react';
import '../Ingridients/Ingredients.css';
import Button from "../Button/Button";

const Ingredients = props => {
    return (
               <li className='flex'>
                   <Button
                       picture={props.picture}
                       add={props.add}
                   />
                   <span>{props.name}</span>
                   <span>x{props.counter}</span>
                   <button className='delete' onClick={props.remove}
                           style={{height: '25px',
                       background: 'indianred',
                       border:'indianred',
                       color: 'white'}}>
                       &#10007;
                   </button>
               </li>
    );
};

export default Ingredients;