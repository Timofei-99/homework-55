import React from 'react';

const Button = props => {
    return (
        <button className='picBtn' onClick={props.add} style={{marginRight: '30px', borderRadius: '30%'}}>
            <img src={props.picture} alt='' style={{width: '30px'}}/>
        </button>
    );
};

export default Button;