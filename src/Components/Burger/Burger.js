import React from 'react';
import '../Burger/Burger.css';

const Burger = props => {
    return (
        <div className='myBurger'>
            <h2 style={{textAlign: 'center'}}>Burger:</h2>
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>
            {props.burg.map(el => {
                const newArray = []
                for (let i = 0; i < el.count; i++)
                    newArray.push(<div className={el.name} key={i}></div>);
                return newArray;
            })}
            <div className="BreadBottom"></div>
        </div>
            <h3 className='Total'>Price: {props.total}</h3>
        </div>
    );
};

export default Burger;

