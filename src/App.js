import './App.css';
import Burger from "./Components/Burger/Burger";
import {useState} from "react";
import {nanoid} from 'nanoid';
import meatImage from '../src/assets/Без названия.jpeg';
import cheeseImage from '../src/assets/cheese (1).jpeg';
import saladImage from '../src/assets/salad.jpeg';
import baconImage from '../src/assets/bacon.jpeg';
import Ingridients from "./Components/Ingridients/Ingridients";

function App() {
    const [ingredients, setIngredients] = useState([]);
    const [price, setPrice] = useState(20);


    const INGREDIENTS = [
        {name: 'Meat', price: 50, image: meatImage},
        {name: 'Cheese', price: 20, image: cheeseImage},
        {name: 'Salad', price: 5, image: saladImage},
        {name: 'Bacon', price: 30, image: baconImage},
    ];

    if (ingredients.length === 0) {
        setIngredients([
            {name:'Meat', count: 0, id: nanoid()},
            {name:'Cheese', count: 0, id: nanoid()},
            {name:'Salad', count: 0, id: nanoid()},
            {name:'Bacon', count: 0, id: nanoid()},
        ]);
    }


    const addElem = name => {
        setIngredients(ingredients.map((el, i) => {
            if (el.name === name) {
                setPrice(price + INGREDIENTS[i].price);
                return {...el, count: el.count + 1, id: nanoid(5)};
            }
            return el
        }));
    };
    console.log(ingredients);

    const deleteElem = name => {
        setIngredients(ingredients.map((el, i) => {
            if (el.name === name) {
                if (el.count !== 0) {
                    setPrice(price - INGREDIENTS[i].price)
                    return {...el, count: el.count - 1}
                }
            }
            return el
        }));
    };


    const ings = ingredients.map((p, i) => (
        <Ingridients name={p.name}
                     counter={p.count}
                     key={p.name}
                     picture={INGREDIENTS[i].image}
                     add={() => addElem(p.name)}
                     remove={() => deleteElem(p.name)}
        />
    ));


  return (
    <div className="App">
        <div className="myIngs">
            <h2 style={{textAlign: 'center', marginBottom:'50px'}}>Ingredients:</h2>
            <ul className='ings'>
                {ings}
            </ul>

        </div>
      <Burger burg={ingredients} total={price}/>
    </div>
  );
}

export default App;
